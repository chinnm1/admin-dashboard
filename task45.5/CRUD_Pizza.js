'use strict';
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gListOrders = [];//khai báo biến mảng chứa thông tin đơn hàng  
    var gListUpdate = [];// khai báo mảng chứa order update
    const gCOLNAME = ['orderCode', 'kichCo', 'loaiPizza', 'idLoaiNuocUong', 'thanhTien', 'hoTen', 'soDienThoai', 'trangThai', 'action']
    const gORDER_CODE_COL = 0
    const gKICH_CO_COL = 1
    const gLOAI_PIZZA_COL = 2
    const gLOAI_NUOC_UONG_COL = 3
    const gTHANH_TIEN_COL = 4
    const gHO_TEN_COL = 5
    const gSO_DIEN_THOAI_COL = 6
    const gTRANG_THAI_COL = 7
    const gACTION_COL = 8
    var gTable = $('#table-order').DataTable({
        columns: [
            { data: gCOLNAME[gORDER_CODE_COL] },
            { data: gCOLNAME[gKICH_CO_COL] },
            { data: gCOLNAME[gLOAI_PIZZA_COL] },
            { data: gCOLNAME[gLOAI_NUOC_UONG_COL] },
            { data: gCOLNAME[gTHANH_TIEN_COL] },
            { data: gCOLNAME[gHO_TEN_COL] },
            { data: gCOLNAME[gSO_DIEN_THOAI_COL] },
            { data: gCOLNAME[gTRANG_THAI_COL] },
            { data: gCOLNAME[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: `<button class = 'btn btn-primary btn-detail'>Chi tiết</button>`
            }
        ]

    })
    var gId; //biến cục bộ chứa id của order
    var gOrderCode; // biến cục bộ chứa order code
    var gPizzaSize = ['S', 'M', 'L']// khai báo mảng chứa kích cỡ pizza
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    //thêm dữ liệu vào bảng
    loadDataToTable(gListOrders);
    //hàm gọi api để load dữ liệu vào selectdrink
    loadDrinkToSelect();
    //hàm load dữ liệu vào kích cỡ
    loadSizeToSelect(gPizzaSize);
    //gán sự kiện nút chi tiết
    $('#table-order').on('click', '.btn-detail', function () {
        onBtnDetailClick(this);
    })
    //gán sự kiện nút lọc
    $('#btn-filter').on('click', function () {

        onBtnFilterClick();
    })
    //gán sự kiện nút confirm trên modal
    $('#btn-confirm').on('click', function () {
        onBtnConfirmClick()


    })
    //gán sự kiện nút cancel trên modal
    $('#btn-cancel').on('click', function () {
        onBtnCancelClick()


    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        //lấy data từ server
        $.ajax({
            async: false,
            url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
            type: "GET",
            dataType: 'json',
            success: function (res) {
                console.log('>>>> check data order');
                console.log(res)
                gListOrders = res;

            },
            error: function (ajaxContent) {
                alert(ajaxContent.responseText);
            }
        })
    }
    //hàm thực thi khi bấm nút chi tiết
    function onBtnDetailClick(paramElementButton) {
        'use strict';
        $('#modal-detail').modal('show');
        var vRow = $(paramElementButton).parents('tr');
        var vOrderData = gTable.row(vRow).data();
        var vId = vOrderData.id;
        gId = vId;
        var vOrderCode = vOrderData.orderCode;
        gOrderCode = vOrderCode;
        var vKichCo = vOrderData.kichCo;
        var vLoaiPizza = vOrderData.loaiPizza;
        var vIdLoaiNuocUong = vOrderData.idLoaiNuocUong;
        var vThanhTien = vOrderData.thanhTien;
        var vHoTen = vOrderData.hoTen;
        var vTrangThai = vOrderData.trangThai;
        console.log(vId);
        console.log(vOrderCode);
        console.log(vKichCo);
        console.log(vLoaiPizza);
        console.log(vIdLoaiNuocUong);
        console.log(vThanhTien);
        console.log(vHoTen);
        console.log(vTrangThai);
        getOrrderByOrderCode(gOrderCode);

    }
    //hàm thực thi khi ấn nút lọc dữ liệu
    function onBtnFilterClick() {
        //khai báo object chứ dữ liệu
        var vObjectFilter = new Object();
        //B1 thu thập dữ liệu
        vObjectFilter.trangThai = $('#sel-status').val();
        vObjectFilter.loaiPizza = $('#sel-pizza').val();
        //B2 validate dữ liệu
        if (vObjectFilter.trangThai === 'all' && vObjectFilter.loaiPizza === 'all') {
            alert('Hãy chọn trạng thái hoặc loại pizza muốn tìm !');
        }
        else {
            var vListFilterObject = gListOrders.filter((order) => {
                return (vObjectFilter.trangThai === 'all' || vObjectFilter.trangThai.toUpperCase() === order.trangThai.toUpperCase())
                    && (vObjectFilter.loaiPizza === 'all' || vObjectFilter.loaiPizza.toUpperCase() === order.loaiPizza.toUpperCase())
            });
            //gọi hàm load dữ liệu
            loadDataToTable(vListFilterObject);
        }

    }
    //hàm thực thi khi ân nút confirm trên modal
    function onBtnConfirmClick() {
        //khai báo biến object chứa thông tin confirn
        var vOrderConfirm = {
            trangThai: 'confirmed',
        }
        //B2 validate dữ liệu
        //B3 gọi api để upload dữ liệu
        upDateConfirmData(vOrderConfirm);
        //xóa trắng bảng
        // ResetTable();
        //tải lại trang
        // callAjaxToGetData();
        onPageLoading()
        console.log('>>>check mảng order');
        console.log(gListOrders);
        //load dữ liệu vào bảng
        loadDataToTable(gListOrders);
        //đóng modal
        $('#modal-detail').modal('hide');


    }

    //hàm thực thi khi ân nút cancel trên modal
    function onBtnCancelClick() {
        //khai báo biến object chứa thông tin confirn
        var vOrderCancel = {
            trangThai: 'cancel',
        }
        //B2 validate dữ liệu
        //B3 gọi api để upload dữ liệu
        upDateCancelData(vOrderCancel);
        //xóa trắng bảng
        // ResetTable();
        //tải lại trang
        // callAjaxToGetData();
        onPageLoading()
        console.log('>>>check mảng order');
        console.log(gListOrders);
        //load dữ liệu vào bảng
        loadDataToTable(gListOrders);
        //đóng modal
        $('#modal-detail').modal('hide');



    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    //hàm hiển  thị dữ liệu ra bảng
    function loadDataToTable(paramOrderObject) {
        'use strict';
        //xóa trắng bảng
        gTable.clear();
        //load dữ liệu vào bảng
        gTable.rows.add(paramOrderObject);
        //thêm vào từng dòng
        gTable.draw();

    }


    //hàm gọi api để load dữ liệu vào select drink
    function loadDrinkToSelect() {
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
            type: "GET",
            dataType: 'json',
            success: function (res) {
                console.log('>>>> check data drink')
                console.log(res);
                displayDrinkOnSelect(res)
            },
            error: function (ajaxContent) {
                alert(ajaxContent.responseText);
            }
        })
    }
    //hàm hiển thị data drink trên select drink
    function displayDrinkOnSelect(paramDrinkObject) {
        'use strct';
        $.each(paramDrinkObject, (index, item) => {

            $('#select-loai-do-uong').append($('<option/>', {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }))
        })
    }
    //hàm load size pizza vào select kích cỡ
    function loadSizeToSelect(paramSize) {
        'use strict';
        $.each(paramSize, (index, item) => {
            $('#select-pizza-size').append($('<option/>', {
                text: item,
                value: item
            }))
        })
    }
    //hàm gọi api để lấy thông tin order theo ordercode
    function getOrrderByOrderCode(paramOrderCode) {
        'use strict'
        $.ajax({

            url: "http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramOrderCode,
            type: "GET",
            dataType: 'json',
            success: function (res) {
                console.log('>>>check data của order được chọn');
                console.log(res);
                displayOrderDetailOnModal(res);
            },
            error: function (ajaxContent) {
                alert(ajaxContent.responseText);
            }
        })
    }
    //hàm hiển thị thông tin order lên modal
    function displayOrderDetailOnModal(paramOrderDetail) {
        'use strict';
        $('#input-order-code').val(paramOrderDetail.orderCode);
        $('#select-pizza-size').val(paramOrderDetail.kichCo);
        $('#input-duong-kinh').val(paramOrderDetail.duongKinh);
        $('#input-suon').val(paramOrderDetail.suon);
        $('#input-salad').val(paramOrderDetail.salad);
        $('#input-pizza-type').val(paramOrderDetail.loaiPizza);
        $('#input-id-voucher').val(paramOrderDetail.idVourcher);
        $('#input-thanh-tien').val(paramOrderDetail.thanhTien);
        $('#input-giam-gia').val(paramOrderDetail.giamGia);
        $('#select-loai-do-uong').val(paramOrderDetail.idLoaiNuocUong);
        $('#input-so-luong-nuoc-uong').val(paramOrderDetail.soLuongNuoc);
        $('#input-ho-va-ten').val(paramOrderDetail.hoTen);
        $('#input-email').val(paramOrderDetail.email);
        $('#input-so-dien-thoai').val(paramOrderDetail.soDienThoai);
        $('#input-dia-chỉ').val(paramOrderDetail.diaChi);
        $('#input-loi-nhan').val(paramOrderDetail.loiNhan);
        $('#input-ngay-order').val(paramOrderDetail.ngayTao);
        $('#input-ngay-chinh-sua').val(paramOrderDetail.ngayCapNhat)
    }
    //hàm gọi api để update dữ liệu
    function upDateConfirmData(paramOrder) {
        $.ajax({
            async: false,
            url: "http://203.171.20.210:8080/devcamp-pizza365/orders/" + gId,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrder),
            success: function (res) {
                console.log('>>>check trạng thái của order được update');
                console.log(res.trangThai);
                alert('confirm dữ liệu thành công')
            },
            error: function (ajaxContent) {
                alert(ajaxContent.responseText);
            }
        })

    }
    //hàm gọi api để update dữ liệu
    function upDateCancelData(paramOrder) {

        $.ajax({
            async: false,
            url: "http://203.171.20.210:8080/devcamp-pizza365/orders/" + gId,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrder),
            success: function (res) {
                console.log('>>>check trạng thái của order được update');
                console.log(res.trangThai);
                alert('cancel dữ liệu thành công')
            },
            error: function (ajaxContent) {
                alert(ajaxContent.responseText);
            }
        })

    }
    // //hàm gọi lại api để load dữ liệu
    // function callAjaxToGetData() {
    //     //lấy data từ server
    //     $.ajax({
    //         async: false,
    //         url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
    //         type: "GET",
    //         dataType: 'json',
    //         success: function (res) {
    //             console.log('>>>> check data order');
    //             console.log(res)
    //             gListUpdate = res;
    //         },
    //         error: function (ajaxContent) {
    //             alert(ajaxContent.responseText);
    //         }
    //     })

    // }
    // //hàm xóa trắng bảng
    // function ResetTable() {
    //     $('#course-table').DataTable().clear().draw();
    // }

});