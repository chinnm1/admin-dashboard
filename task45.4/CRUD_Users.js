$(document).ready(function () {
    "use strict";

    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Biến toàn cục để lưu trữ id userđang đc update or delete. Mặc định = 0;
    var guserId = 0;
    //Khai báo mảng toàn cục chứa user object
    var gListUsers = [];
    var gCountryObject = [
        {
            'value': 'VN',
            'text': 'Việt Nam'
        },
        {
            'value': 'USA',
            'text': 'ÚA'
        },

        {
            'value': 'AUS',
            'text': 'Australia'
        },
        {
            'value': 'CAN',
            'text': 'Canada'
        }




    ]
    const gNOT_SELECT_COUNTRY = 'NOT_SELECT_COUNTRY'; // giá trị value khi chưa chọn country
    const gNOT_SELECT_CUSTOMER_TYPE = 'NOT_SELECT_CUSTOMER_TYPE'; // giá trị value khi chưa chọn customer type
    const gNOT_SELECT_REGISTER_STATUS = 'NOT_SELECT_REGISTER_STATUS'; // giá trị value khi chưa chọn regisster status
    //Khai báo biến mảng chứa danh sách tên các thuộc tính
    const gUSER_COLS = ['id', 'firstname', 'lastname', 'country', 'subject', 'customerType', 'registerStatus', 'action']
    //BIến toàn cục định nghĩa các chỉ số cột tương ứng
    const gUSER_ID_COL = 0;
    const gUSER_FIRSTNAME_COL = 1;
    const gUSER_LASTNAME_COL = 2;
    const gUSER_COUNTRY_COL = 3;
    const gUSER_SUBJECT_COL = 4;
    const gUSER_CUSTOMER_TYPE_COL = 5;
    const gUSER_REGISTER_STATUS_COL = 6;
    const gUSER_ACTION_COL = 7;
    //Đổ dữ liệu vào mảng và thực hiện nút Sửa(edit) Xóa (delete)
    $("#user-table").DataTable({
        columns: [
            { data: gUSER_COLS[gUSER_ID_COL] },
            { data: gUSER_COLS[gUSER_FIRSTNAME_COL] },
            { data: gUSER_COLS[gUSER_LASTNAME_COL] },
            { data: gUSER_COLS[gUSER_COUNTRY_COL] },
            { data: gUSER_COLS[gUSER_SUBJECT_COL] },
            { data: gUSER_COLS[gUSER_CUSTOMER_TYPE_COL] },
            { data: gUSER_COLS[gUSER_REGISTER_STATUS_COL] },
            { data: gUSER_COLS[gUSER_ACTION_COL] },
        ],
        columnDefs: [
            {
                //Định nghĩa lại cột country
                targets: gUSER_COUNTRY_COL,
                render: function (data) {
                    return getCountryTextByCountryValue(data);

                }
            },
            {
                //Định nghĩa lại cột Action
                targets: gUSER_ACTION_COL,
                className: 'text-center',
                defaultContent:
                    `<button class="btn btn-primary btn-edit">Sửa</button>
                <button class="btn btn-danger btn-delete" data-toggle='modal'>Xóa</button>`

            }
        ],
        autoWidth: false
    })
    //khai báo mảng chưa thông tin của tất cả user
    var gListUsers = [];
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();
    //2-C: Gán sự kiện create thêm mới user
    $('#btn-add-user').on('click', function () {
        onBtnAddNewUserClick();
    })
    //Gán sự kiện cho nút insert user trên create modal
    $('#btn-create-user').on('click', function () {
        onBtnCreateUserClick();
    })
    //3-U: gán sự kiện update- Sửa 1 user
    $('#user-table').on('click', '.btn-edit', function () {
        onBtnEditClick(this);
    })
    // gán sự kiện cho nút Update user (trên modal)
    $("#btn-update-user").on("click", function () {
        onBtnUpdateuserClick();
    });
    //4-D: gán sự kiện delete- Sửa 1 user
    $('#user-table').on('click', '.btn-delete', function () {
        onBtnDeleteClick(this);
    })
    // gán sự kiện cho nút confirm delete (trên modal)
    $("#btn-confirm-delete").on("click", function () {
        onBtnConfirmDeleteClick();
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //Hàm thực được thực thi khi load trang
    function onPageLoading() {
        "use strict";

        getAllUsers();

        loadDataToUserTable(gListUsers);

    }
    //Hàm xử lí sự kiện nút thêm mới user
    function onBtnAddNewUserClick() {
        //hiển thị modal trắng lên
        $('#create-user-modal').modal('show');
    }
    //Hàm xử lí sự kiện click nút insert user
    function onBtnCreateUserClick() {
        var vUserObject = {
            firstname: '',
            lastname: '',
            subject: '',
            country: ''
        }
        //B1 thu thập dữ liệu
        getUserDataCreate(vUserObject);
        //B2 validate insert
        var vIsUserValidate = validateInsertUser(vUserObject);
        if (vIsUserValidate) {
            //B3 Insert user
            postUserApi(vUserObject);
            //load lại vào bảng (table)
            //B4 xử lí front-end
            getAllUsers();
            loadDataToUserTable(gListUsers);
            resertUpdateuserForm();
            //Xóa trắng dữ liệu trên modal
            resertCreateUserForm();
            //Tắt (ẩn) modal form crate user
            $("#create-user-modal").modal('hide');
        }
    }
    //Hàm xử lí sự kiện nút click nút sửa
    function onBtnEditClick(paramButton) {
        'use strict';

        // lưu thông tin userId đang được edit vào biến toàn cục
        guserId = getUserIdFromButton(paramButton)
        console.log(guserId);
        // load dữ liệu vào các trường dữ liệu trong modal
        showUserDataToModal(guserId);
        //hiển thị modal trắng lên
        $('#update-user-modal').modal('show');
    }
    // hàm xử lý sự kiện update user modal click
    function onBtnUpdateuserClick() {
        // khai báo đối tượng chứa user data
        var vUserObject = {
            id: 0,
            firstname: '',
            lastname: '',
            subject: '',
            country: '',
            customerType: '',
            registerStatus: ''
        };
        // B1: Thu thập dữ liệu
        getUpdateUserData(vUserObject);
        // B2: Validate update
        var vIsUserValidate = validateuserData(vUserObject);
        if (vIsUserValidate) {
            // B3: update user
            updateuser(vUserObject);
            // B4: xử lý front-end

            getAllUsers();
            loadDataToUserTable(gListUsers);
            resertUpdateuserForm();
            $("#update-user-modal").modal("hide");
        }
    }
    //Hàm xử lí sự kiện nút click nút delete
    function onBtnDeleteClick(paramButton) {
        'use strict';
        // lưu thông tin userId đang được edit vào biến toàn cục
        guserId = getUserIdFromButton(paramButton)
        console.log(guserId);
        //hiển thị modal trắng lên
        $('#delete-confirm-modal').modal('show');
    }
    // hàm xử lý sự kiện confirm delete user modal click
    function onBtnConfirmDeleteClick() {
        // khai báo đối tượng chứa voucher data
        // B1: Thu thập dữ liệu
        // B2: Validate delete
        // B3: delete voucher
        deleteUser(guserId);
        // B4: xử lý front-end
        getAllUsers();
        loadDataToUserTable(gListUsers);
        //resertUpdateVoucherForm();
        $("#delete-confirm-modal").modal("hide");
    }




    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // hàm xóa user theo id user
    function deleteUser(paramUserId) {
        // var vUserIndex = getIndexFormUserId(paramUserId);
        // gListUsers.splice(vUserIndex, 1);
        'use strict';
        $.ajax({
            async: false,
            url: "http://203.171.20.210:8080/crud-api/users/" + guserId,
            type: 'DELETE',
            contentType: 'json',
            success: function (res) {

                alert('xóa user thành công');
            },
            error: function (error) {
                alert(error.responseText)
            }
        })
    }

    /*-------------------------------------------------------------------------------------------------------*/
    // hàm thu thập dữ liệu để update user
    function getUpdateUserData(paramuserObj) {
        paramuserObj.id = guserId;
        paramuserObj.firstname = $("#input-update-firstname").val().trim();
        paramuserObj.lastname = $("#input-update-lastname").val().trim();
        paramuserObj.subject = $("#input-update-subject").val().trim();
        paramuserObj.country = $("#select-update-country").val();
        paramuserObj.customerType = $("#select-update-customertype").val();
        paramuserObj.registerStatus = $("#select-update-register-status").val();
    }

    // hàm show userobj lên modal
    function showUserDataToModal(paramUserId) {
        console.log('đang load thông tin');
        var vUserIndex = getIndexFormUserId(paramUserId);
        console.log('user index là;' + vUserIndex)
        $("#input-update-firstname").val(gListUsers[vUserIndex].firstname);
        $("#input-update-lastname").val(gListUsers[vUserIndex].lastname);
        $("#input-update-subject").val(gListUsers[vUserIndex].subject);
        $("#select-update-country").val(gListUsers[vUserIndex].country);
        $("#select-update-customertype").val(gListUsers[vUserIndex].customerType);
        $("#select-update-register-status").val(gListUsers[vUserIndex].registerStatus);

    }
    //hamd validate data khi update
    function validateuserData(paramUserObject) {
        'use strict';
        if (paramUserObject.firstname == '') {
            alert('firstname cần nhập');
            return false;
        }
        if (paramUserObject.lastname == '') {
            alert('lastname cần nhập');
            return false;
        }
        if (paramUserObject.subject == '') {
            alert('subject cần nhập');
            return false;
        }
        if (paramUserObject.country == gNOT_SELECT_COUNTRY) {
            alert('country cần chọn');
            return false;
        }
        if (paramUserObject.customerType == gNOT_SELECT_CUSTOMER_TYPE) {
            alert('customer type cần chọn');
            return false;
        }
        if (paramUserObject.registerStatus == gNOT_SELECT_REGISTER_STATUS) {
            alert('register status cần chọn');
            return false;
        }
        return true;
    }
    // hàm thực hiện update voucher vào mảng
    function updateuser(paramUserObject) {
        // var vUserIndex = getIndexFormUserId(guserId);
        // gListUsers.splice(vUserIndex, 1, paramUserObject);
        'use strict';
        $.ajax({
            async: false,
            url: "http://203.171.20.210:8080/crud-api/users/" + guserId,
            type: 'PUT',
            data: JSON.stringify(paramUserObject),
            contentType: 'application/json',
            success: function (res) {
                console.log(res);
                alert('Thêm user thành công');
            },
            error: function (error) {
                alert(error.responseText)
            }
        })
    }
    // hàm xóa trắng form create voucher
    function resertUpdateuserForm() {
        $("#input-update-firstname").val('');
        $("#input-update-lastname").val('');
        $("#input-update-subject").val('');
        $("#select-update-country").val(gNOT_SELECT_COUNTRY);
        $("#select-update-customertype").val(gNOT_SELECT_CUSTOMER_TYPE);
        $("#select-update-register-status").val(gNOT_SELECT_REGISTER_STATUS);
    }

    /* get userindex from userid
    // input: paramUserId là userId cần tìm index
    / output: trả về chỉ số (index) trong mảng user*/
    function getIndexFormUserId(paramUserId) {
        var vUserIndex = -1;
        var vUserFound = false;
        var vLoopIndex = 0;
        while (!vUserFound && vLoopIndex < gListUsers.length) {
            if (gListUsers[vLoopIndex].id === paramUserId) {
                vUserIndex = vLoopIndex;
                vUserFound = true;
            }
            else {
                vLoopIndex++;
            }
        }
        return vUserIndex;
    }
    /*------------------------------------------------------------------------------*/


    //Hàm thu thập dữ liệu để insert user
    function getUserDataCreate(paramUserObject) {
        'use strict';
        paramUserObject.firstname = $('#input-firstname').val().trim();
        paramUserObject.lastname = $('#input-lastname').val().trim();
        paramUserObject.subject = $('#input-subject').val().trim();
        paramUserObject.country = $('#select-country').val()
    }
    //hamd validate data khi insert
    function validateInsertUser(paramUserObject) {
        'use strict';
        if (paramUserObject.firstname == '') {
            alert('firstname cần nhập');
            return false;
        }
        if (paramUserObject.lastname == '') {
            alert('lastname cần nhập');
            return false;
        }
        if (paramUserObject.subject == '') {
            alert('subject cần nhập');
            return false;
        }
        if (paramUserObject.country == gNOT_SELECT_COUNTRY) {
            alert('country cần chọn');
            return false;
        }
        return true;
    }
    //Hàm gọi Api insert user
    function postUserApi(paramUserObject) {
        'use strict';
        $.ajax({
            async: false,
            url: "http://203.171.20.210:8080/crud-api/users/",
            type: 'POST',
            data: JSON.stringify(paramUserObject),
            contentType: 'application/json',
            success: function (res) {
                console.log(res);
                alert('Thêm user thành công');
            },
            error: function (error) {
                alert(error.responseText)
            }
        })
    }
    //hàm xóa trắng form crate user
    function resertCreateUserForm() {
        $("#input-firstname").val('');
        $("#input-lastname").val('');
        $("#input-subject").val('');
        $("#select-country").val(gNOT_SELECT_COUNTRY);

    }

    /*--------------------------------------------------------------*/
    //Hàm lấy text country theo value của country
    function getCountryTextByCountryValue(paramCountry) {
        'use strict';
        var vTextCountry = '';
        for (var bI = 0; bI < gCountryObject.length; bI++) {
            if (gCountryObject[bI].value == paramCountry) {
                vTextCountry = gCountryObject[bI].text
            }

        };
        return vTextCountry;
    }

    //hàm gọi api để lấy dữ liệu 
    function getAllUsers(paramUserObject) {
        "use strict";

        $.ajax({

            url: "http://203.171.20.210:8080/crud-api/users/",
            type: "GET",
            dataType: "json",
            async: false,

            success: function (res) {

                console.log(res);
                //1-R read/Load user to data table
                gListUsers = res


            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText)
            }
        })

    }
    //hàm xử lí hiện thị thông tin của user ra bảng
    function loadDataToUserTable(paramres) {
        "use strict";

        var vTable = $('#user-table').DataTable();//tạo biến truy xuất đến data table
        vTable.clear();
        vTable.rows.add(paramres);
        vTable.draw();
        return paramres;
    }
    //Hàm thu thập thông tin từ element input name
    function getData(paramres) {
        "use strict";
        paramres.name = $("#inp-name").val().trim();
        return paramres;

    }
    //Hàm kiểm tra dữ liệu name lấy được từ ô input name
    function validateData(paramres) {
        "use strict";
        if (paramres.name == "") {
            alert("Vui lòng nhập tên user !");
            return false;
        }
        return true;
    }

    // hàm dựa vào button detail (edit or delete) xác định đc id voucher
    function getUserIdFromButton(paramButton) {
        var vTableRow = $(paramButton).parents("tr");
        var vUserRowData = $("#user-table").DataTable().row(vTableRow).data();
        return vUserRowData.id;
    }

})
